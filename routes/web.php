<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('makineri', 'Makineri@index');*/


Route::get('atach/{id}', 'MachinesController@atach')->name('atach');
Route::post('update_quantity/{id}', 'MachinesController@update_quantity')->name('update_quantity');
Route::resource('machines','MachinesController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
