@extends('machines.layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Disinfectant Machinery</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('machines.create') }}"> Create New Machine</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Code</th>
            <th>Sasi</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($machines as $machine)
        <tr>
            <td>{{ ++$i }}</td>
            
            <td>{{ $machine->name }}</td>
            <td>{{ $machine->code }}</td>
            <td>{{ $machine->sasi }}</td>

            <td>
                <form action="{{ route('machines.destroy',$machine->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('machines.show',$machine->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('machines.edit',$machine->id) }}">Edit</a>
                    <a class="btn btn-primary" href="{{ route('atach',$machine->id) }}">Atach</a>

                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Sure Want Delete?')">Delete</button>
                
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $machines->links() !!}
      
@endsection