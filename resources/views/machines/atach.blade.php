@extends('machines.layout')
   
@section('content')
<form action="{{ route('update_quantity',$machine->id) }}" method="POST">
@csrf 

<select class="form-control m-bot15" name="client">
          @foreach($clients as $client)
           <option value="{{$client->id}}">{{$client->name}}</option>
          @endForeach
            
        </select>

        <button type="submit" value="Submit">Submit</button>

        </form>
    
@endsection