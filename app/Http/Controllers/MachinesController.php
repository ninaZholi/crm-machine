<?php

namespace App\Http\Controllers;

use App\Machines;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



class MachinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $machines = Machines::where('sasi','!=',0)->paginate(5);

        return view('machines.index',compact('machines'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('machines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            
            'name' => 'required',
            'code' => 'required',
            'sasi'=> 'required',
        ]);
  
        Machines::create($request->all());
   
        return redirect()->route('machines.index')
                        ->with('success','Machine created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Machines  $machines
     * @return \Illuminate\Http\Response
     */
    public function show(Machines $machine)
    {
        return view('machines.show',compact('machine'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Machines  $machines
     * @return \Illuminate\Http\Response
     */
    public function edit(Machines $machine)
    {    
        if(Auth::user()-> role == "operator"){
            abort('404');
            }

        return view('machines.edit',compact('machine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Machines  $machines
     * @return \Illuminate\Http\Response
     */
    public function atach($id)
    {    
        if(Auth::user()-> role == "operator"){
            abort('404');
            }
        $clients = User:: where ('role','client')->get();
        $machine = Machines::find($id);

        return view('machines.atach',compact(['clients','machine']));
    }

    public function update_quantity($id){
        $machine = Machines::find($id)->decrement ('sasi');
        
        return redirect()->route('atach',$id)
        ->with('success','Machine atached successfully');
    }

    public function update(Request $request, Machines $machine)
    {
        $request->validate([
            
            'name',
            'code',
            'sasi',
        ]);
  
        $machine->update($request->all());
        if(Auth::user()-> role == "operator"){
            abort('404');
            }
        return redirect()->route('machines.index')
                        ->with('success','Machine updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Machines  $machines
     * @return \Illuminate\Http\Response
     */
    
    public function destroy(Machines $machine)
    {
        $machine->delete();

        if(Auth::user()-> role == "operator"){
            abort('404');
            }
        return redirect()->route('machines.index')
                        ->with('success','Machine deleted successfully');
    }
}
