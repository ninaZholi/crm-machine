<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Machines extends Model
{
    protected $fillable = [
        'id','name', 'code', 'sasi',
    ];

    public $table ='machines';
}
